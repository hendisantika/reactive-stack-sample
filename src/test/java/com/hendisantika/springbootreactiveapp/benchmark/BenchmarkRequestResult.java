package com.hendisantika.springbootreactiveapp.benchmark;

import org.springframework.http.ResponseEntity;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-app
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-26
 * Time: 05:58
 */
public class BenchmarkRequestResult {
    private final int requestId;
    private final long tookTimeNs;
    private final ResponseEntity<String> responseEntity;

    public BenchmarkRequestResult(final int requestId, final ResponseEntity<String> responseEntity, final long tookTimeNs) {
        this.requestId = requestId;
        this.responseEntity = responseEntity;
        this.tookTimeNs = tookTimeNs;
    }

    public int getRequestId() {
        return requestId;
    }

    public ResponseEntity<String> getResponseEntity() {
        return responseEntity;
    }

    public long getTookTimeNs() {
        return tookTimeNs;
    }

    @Override
    public String toString() {
        return "BenchmarkRequestResult{" +
                "requestId=" + requestId +
                ", tookTimeNs=" + tookTimeNs +
                ", responseEntity=" + responseEntity +
                '}';
    }
}
