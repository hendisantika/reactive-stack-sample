package com.hendisantika.springbootreactiveapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootReactiveAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootReactiveAppApplication.class, args);
    }

}
