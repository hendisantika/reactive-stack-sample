FROM openjdk:8-jre-alpine
MAINTAINER Hendi Santika <hendisantika@yahoo.co.id>
COPY ./target/springboot-reactive-app-1.0.0-SNAPSHOT.jar /usr/src/spring-boot-reactive-web/
WORKDIR /usr/src/spring-boot-reactive-web
CMD ["java", "-jar", "springboot-reactive-app-1.0.0-SNAPSHOT.jar"]