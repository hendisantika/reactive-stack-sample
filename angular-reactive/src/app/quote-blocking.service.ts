import {Injectable} from '@angular/core';
import {Quote} from "./quote";

@Injectable({
  providedIn: 'root'
})
export class QuoteBlockingService {

  constructor() {
  }

  quotes: Quote[] = [];
  url: string = 'http://localhost:8080/quotes-blocking';
  urlPaged: string = 'http://localhost:8080/quotes-blocking-paged';
}
