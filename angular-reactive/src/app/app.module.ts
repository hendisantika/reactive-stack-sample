import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {QuotesComponentComponent} from './quotes-component/quotes-component.component';
import {QuoteDetailComponent} from './quote-detail/quote-detail.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {QuoteReactiveService} from "./quote-reactive.service";
import {QuoteBlockingService} from "./quote-blocking.service";

@NgModule({
  declarations: [
    AppComponent,
    QuotesComponentComponent,
    QuoteDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    QuoteReactiveService,
    QuoteBlockingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
